//Schemas
var User = require( '../db/models/userSchema.js' );

/**
*
* @param      {String} type - type of the user
*/
exports.addUser = function( req, res, next ) {

	var user = new User({ });
	//save model to MongoDB
	user.save( function( err, vehicle ) {
		if ( err ) {
			next(new Error( "Could not add User. Please try again" ));
		} else {
			console.log( "User added" );
			res.json( user._id );
		}
	});
}
