var express = require('express');
var userRoutes = express.Router();
var jwt = require('jsonwebtoken');
var User = require('../db/models/userSchema');

// ---------------------------------------------------------
// route middleware to authenticate and check token
// ---------------------------------------------------------

userRoutes.use(function(req, res, next) {

    var token = req.body.token || req.param('token') || req.headers['x-access-token'];

    if (token) {
        jwt.verify(token, 'khfkjbsakjfkjsdakjfb', function(err, decoded) {
            if (err) {
                return res.json({success: false, message: 'Failed to authenticate token.'});
            } else {
                req.decoded = decoded;
                next();
            }
        });

    } else {
        return res.status(401).send({success: false, message: 'No token provided.'});

    }

})

module.exports = function(app) {

    userRoutes.post('/:id/add/phone', function(req, res) {
        var regex = /^\d{10}$/;
        var id = req.params.id;
        var phone = req.body.phone;
        if (regex.test(phone)) {
            User.findById(id, function(err, user) {
                if (err || !user)
                    res.status(500).send({success: false, message: 'Error fetching user'});
                else {

                    User.findOne({
                        phone: phone
                    }, function(err, doc) {
                        if (err)
                            res.status(500).send({success: false, message: 'Error Finding user'});
                        else if (doc)
                            res.status(500).send({success: false, message: 'Phone No. already registered with another account.'});
                        else {
                            user.phone = phone;
                            user.save(function(err, user) {
                                if (err)
                                    res.status(500).send({success: false, message: 'Error Updating user'});
                                if (!users)
                                    res.status(500).send({success: false, message: 'Error Updating user'});
                                else
                                    res.send({success: true, verified: true, userToken: user.generateJwt()});
                                }
                            )

                        }
                    })

                }
            })
        } else {
            res.send({working: true, id: req.params.id});
        }
    });

    userRoutes.post('/test', function(req, res) {
        res.send({working: true});
    });

    app.use('/user', userRoutes);
}
