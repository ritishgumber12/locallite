var express = require('express');
var routes = express.Router();
var User = require('../db/models/userSchema');

module.exports = function(app) {

    routes.post('/save/token', function(req, res) {
        var id = req.body.id,
            email = req.body.email,
            name = req.body.name,
            profilePic = req.body.profilePic,
            provider = req.body.provider,
            token = req.body.token;

        if (!id || !email || !name || !provider || !token || !profilePic)
            res.status(400).send({success: false, message: 'Parameter missing'});
        else if (_validateId(id) && _validateEmail(email) && _validateName(name) && _validateProvider(provider) && _validateProfilePic(profilePic)) {
            User.find({
                providerId: id,
                provider: provider
            }, function(err, users) {
                if (err)
                    res.status(500).send({
                        success: false,
                        message: 'Err' + err
                    });
                if (!users)
                    res.send({success: false});
                else if (users.length === 1) {
                    // user with same id and providerfound in database
                    if (!users[0].phone)
                        res.send({success: true, verified: false, userToken: users[0].generateJwt()});
                    else
                        res.send({success: true, verified: true, userToken: users[0].generateJwt()});
                    }
                else {
                    //search if any user exists with same email id of any other provider.
                    User.find({
                        email: email
                    }, function(err, users) {
                        if (err)
                            res.status(500).send({
                                success: false,
                                message: 'Err' + err
                            });
                        else if (!users)
                            res.status(500).send({success: false});
                        else if (users.length === 1) {
                            var user = users[0];
                            var verified = false;
                            if (user.phone)
                                verified = true;

                            //                            user.provider.push(provider);
                            user.provider.set(1, provider);
                            //                          user.providerId.push(id);
                            user.providerId.set(1, id);
                            //                            user.providerToken.push(token);
                            user.providerToken.set(1, token);
                            user.save(function(err, user) {
                                if (err)
                                    res.status(500).send({success: false, message: 'Error Updating user'});
                                if (!users)
                                    res.status(500).send({success: false, message: 'Error Updating user'});
                                else
                                    res.send({success: true, verified: verified, userToken: user.generateJwt()});
                                }
                            )
                        } else {
                            //No User exists with the provided email id or provide id, so create a new user;
                            var user = new User();
                            user.name = name;
                            user.email = email;
                            user.profilePic = profilePic;
                            user.provider = provider;
                            user.providerId = id;
                            user.providerToken = token;
                            user.save(function(err, user) {
                                if (err)
                                    res.status(500).send({success: false, message: 'Error Saving user'});
                                if (!users)
                                    res.status(500).send({success: false, message: 'Error saving user'});
                                else
                                    res.send({success: true, verified: false, userToken: user.generateJwt()});
                                }
                            )
                        }
                    })
                }
            })
        } else
            res.status(400).send({success: false, message: 'Parameter invalid'});
        }
    );

    routes.get('/version', function(req, res) {
        res.send({_v: 1.0});
    })

    app.use('/api', routes);
}

function _validateId(id) {
    var reg = /^\d+$/;
    return reg.test(id);
}

function _validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function _validateName(name) {
    var regex = /^[a-zA-Z ]{2,30}$/;
    return regex.test(name);
}

function _validateProvider(provider) {
    if (provider === 'google' || provider === 'facebook')
        return true;
    return false;
}

function _validateProfilePic(url) {
    var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
    var regex = new RegExp(expression);
    return regex.test(url);
}
