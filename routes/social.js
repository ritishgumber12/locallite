module.exports = function(app, passport) {

    app.get('/auth/facebook', passport.authenticate('facebook', {scope: 'email'}));
    app.get('/auth/facebook/callback', passport.authenticate('facebook', {
        //      successRedirect: '/',
        failureRedirect: '/error',
        session: false

    }), function(req, res) {

        var token = req.user.generateJwt();
        res.send(token);
    });
    app.get('/auth/google', passport.authenticate('google', {
        scope: ['profile', 'email']
    }));
    app.get('/auth/google/callback', passport.authenticate('google', {
        session: false,
        failureRedirect: '/error'
    }), function(req, res) {

        var token = req.user.generateJwt();
        res.send(token);
    });

};
