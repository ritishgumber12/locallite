var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var basicAuth = require('basic-auth');
var flash = require('connect-flash');
var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;
var https = require('https');
var http = require('http');
var fs = require('fs');
var helmet = require('helmet');
var utils = require('./utils.js');
var bodyParser = require('body-parser');

//Mongoose
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;
var mongodbURI = 'mongodb://127.0.0.1/locallite';
if (process.env.isHosted == true || process.env.isHosted == 'true')
    mongodbURI = process.env.MONGODB_URI;
console.log(mongodbURI);
mongoose.connect(mongodbURI);

//Passport
var passport = require('passport');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var jwt = require('jsonwebtoken');
const secret = 'locallite';

var app = express();
app.use(express.static(path.join(__dirname, 'public')));
app.use(helmet());
var sixtyDaysInSeconds = 5184000;
app.use(helmet.hsts({maxAge: sixtyDaysInSeconds}));

//Allow all requests from all domains & localhost. For CORS requests and such. Remove when deploying.
app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override,Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "POST, GET,OPTIONS");
    next();
});

app.use(require('cookie-parser')());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json())

app.use(passport.initialize());
require("./config/passport")(passport, app);

app.get('/test', function(req, res) {
    res.send('I am working baby');
});
//import routes

require('./routes/routes')(app);
require('./routes/user')(app);
require('./routes/social')(app, passport);

http.createServer(app).listen(process.env.PORT || 3000, function() {
    console.log("localite running at : 3000");
});
