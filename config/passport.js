var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var configAuth = require('./auth');
var User = require('../db/models/userSchema');
module.exports = function(passport, app) {
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });
    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use(new GoogleStrategy({
        clientID: configAuth.googleAuth.clientID,
        clientSecret: configAuth.googleAuth.clientSecret,
        callbackURL: configAuth.googleAuth.callbackURL
    }, function(token, refreshToken, profile, done) {
        // make the code asynchronous
        // User.findOne won't fire until we have all our data back from Google
        process.nextTick(function() {
            // try to find the user based on their google id
            User.findOne({
                'providerid': profile.id
            }, function(err, user) {
                if (err)
                    return done(err);
                if (user) {
                    console.log('old');
                    return done(null, user);
                } else {
                    console.log('new');
                    // if the user isnt in our database, create a new user
                    var newUser = new User();
                    newUser.email = profile.emails[0].value; // pull the first email
                    newUser.loginProvider = 'google';
                    newUser.providerid = profile.id;
                    newUser.providerToken = token;
                    newUser.name = profile.displayName;
                    newUser.dp = profile.photos[0].value;
                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }
            });
        });
    }));
    passport.use(new FacebookStrategy({
        clientID: configAuth.facebookAuth.clientID,
        clientSecret: configAuth.facebookAuth.clientSecret,
        callbackURL: configAuth.facebookAuth.callbackURL,
        profileFields: ["emails", "displayName"]
    },
    // facebook will send back the token and profile
    function(token, refreshToken, profile, done) {
        // asynchronousx
        process.nextTick(function() {
            // find the user in the database based on their facebook id
            User.findOne({
                'providerid': profile.id
            }, function(err, user) {
                if (err)
                    return done(err);
                if (user) {
                    console.log('old');
                    return done(null, user);

                } else {
                    console.log('new');
                    var newUser = new User();
                    newUser.email = profile.emails[0].value; // pull the first email
                    newUser.loginProvider = 'facebook';
                    newUser.providerid = profile.id;
                    newUser.providerToken = token;
                    newUser.name = profile.displayName;
                    newUser.dp = "http://graph.facebook.com/" + profile.id + "/picture?type=large";
                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });

                }
            });
        });
    }));
};
