var mongoose = require("mongoose");
var jwt = require('jsonwebtoken');

var schema = {
    name: {
        required: true,
        type: String
    },
    email: {
        type: String,
        required: true,
        unique: true,
        sparse: true
    },
    phone: {
        type: Number
    },
    latitude: {
        type: Number
    },
    longitude: {
        type: Number
    },
    city: {
        type: String
    },
    profilePic: {
        type: String
    },
    provider: [
        {
            type: String,
            required: true
        }
    ],
    providerId: [
        {
            type: String,
            required: true
        }
    ],
    providerToken: [
        {
            type: String,
            required: true
        }
    ]

};
var schema = new mongoose.Schema(schema);
schema.methods.generateJwt = function() {
    var expiry = new Date();

    return jwt.sign({
        document: this,
        exp: parseInt((expiry.getTime() + 2 * 60 * 60 * 1000) / 1000)
    }, 'khfkjbsakjfkjsdakjfb'); // DO NOT KEEP YOUR SECRET IN THE CODE!
};
module.exports = mongoose.model("User", schema);
